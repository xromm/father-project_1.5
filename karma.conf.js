module.exports = function (config) {
  config.set({
    // конфигурация репортов о покрытии кода тестами
    coverageReporter: {
      dir: 'tmp/coverage/',
      reporters: [
        { type: 'html', subdir: 'report-html' },
        { type: 'lcov', subdir: 'report-lcov' }
      ],
      instrumenterOptions: {
        istanbul: { noCompact: true }
      }
    },
    // spec файлы, условимся называть по маске **_*.spec.js_**
    files: [
      'server/**/*.spec.js'
    ],
    preprocessors: {
      'server/**/*.spec.js': ['webpack', 'sourcemap']
    },
    frameworks: ['jasmine'],

    // репортеры необходимы для  наглядного отображения результатов
    reporters: ['spec'],
    specReporter: {
      maxLogLines: 5, // limit number of lines logged per test
      suppressErrorSummary: true, // do not print error summary
      suppressFailed: false, // do not print information about failed tests
      suppressPassed: false, // do not print information about passed tests
      suppressSkipped: true, // do not print information about skipped tests
      showSpecTiming: true, // print the time elapsed for each spec
      failFast: true // test would finish with error when a first fail occurs.
    },

    browsers: ['PhantomJS2'],
    plugins: [
      'karma-jasmine',
      'karma-spec-reporter',
      'karma-webpack',
      'karma-phantomjs2-launcher',
      'karma-sourcemap-loader'
    ],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,

    webpack: {
      module: {
        loaders: [{
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        }]
      },
      resolve: {
        extensions: ['', '.js']
      }
    },
    webpackMiddleware: {
      noInfo: true
    }
  });
};
