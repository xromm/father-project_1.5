import app from './server';
import config from './config/config';

const port = config.server.port;

const run = async () => {
  const info = await app.listen(port);

  console.log(`\t>>> 🌎 <<< OK, name ${info.name}, port ${port}`); // ${info.host}:${info.port}/${info.name}
};

run();
