import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

let db;

const waitDatabase = () => new Promise((resolve, reject) => {
  // 0 = disconnected
  // 1 = connected
  // 2 = connecting
  // 3 = disconnecting

  let i = 0;
  const weitSize = 1000000;
  const size = 300 * weitSize;

  while (
    (mongoose.connection.readyState === 2
    || mongoose.connection.readyState === 3)
    && (i < size)
  ) {
    if (i % (6 * weitSize) === 0) {
      process.stdout.write('#');
    }
    i += 1;
  }

  if (i < size) {
    resolve(mongoose.connection.readyState);
  }
  reject('OutOfTime');
});

/**
 * Connect to the Database
 * @param  {string} url [URL for connectiong to the database]
 * @return {Promise}     [resolve - open, reject - error]
 */
const connectDatabase = url => new Promise((resolve, reject) => {
  mongoose.connection
    .on('error', (error) => {
      // console.log('\tmongoose on "error"');
      reject(error);
    })
    .once('close', () => console.log('\tmongoose once "close"'))
    .once('open', () => {
      // console.log('mongoose once "open"');
      resolve(mongoose.connections[0]);
    });

  db = mongoose.connect(url, {
    options: {
      server: {
        reconnectTries: 2,
        socketOptions: {
          keepAlive: 20
        }
      }
    }
  });
});

/**
 * close Database
 * @param  {null} ) []
 * @return {Promise}   [resolve - if db disconnected]
 */
export const closeDatabase = () => new Promise((resolve) => {
  mongoose.connection.close();
  mongoose.disconnect(resolve);
});

/**
 * connect to the database by URL and Port
 * @param  {string} url  [URL for connecting to the DB]
 * @return {number}      [connected_1-OK, alreadyConnected_1-warning, (-1)-Error]
 */
export const startDatabase = async (url) => {
  try {
    const status = await waitDatabase();

    if (status === 0) {
      await connectDatabase(url);
      return `connected_${mongoose.connection.readyState}`;
    }

    return `alreadyConnected_${mongoose.connection.readyState}`;
  } catch (error) {
    console.error(error);
    return -1;
  }
};