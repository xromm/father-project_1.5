// import mongoose from 'mongoose';
// import config from '../config/config';
// import { expect } from 'chai';

// import {
//   startDatabase,
//   closeDatabase
// } from './index';

// describe('Database check connection to the database', () => {
//   before(async () => await closeDatabase());
//   afterEach(async () => await closeDatabase());

//   it('Correct URL and check reconnecting property', async () => {
//     const url = config.db.url;

//     const firstTry = await startDatabase(url);
//     expect(firstTry).to.equal('connected_1');
//     const result = await startDatabase(url);
//     expect(result).to.equal('alreadyConnected_1');
//   });

//   it('WRONG database URL', async () => {
//     const url = `${config.db.url}WRONGURL`;

//     const result = await startDatabase(url);
//     expect(result).to.equal(-1);
//   });
// });
