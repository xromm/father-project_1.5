import Router from 'koa-router';
import article from '../routes/article';

const router = new Router();

router
  .post('/article', article.create)
  .put('/article/:id', article.update)
  .get('/article', article.getall)
  .get('/article/:id', article.get)
  .delete('/article/:id', article.delete);

export default router.routes();