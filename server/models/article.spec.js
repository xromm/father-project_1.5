import Article from './article';
import mongoose from 'mongoose';
import { expect } from 'chai';
import * as Immutable from 'immutable';

const correctArticleData = Immutable.Map({
  updateDate: 'updateDate',
  date: 'date',
  nameId: 123,
  name: 'Chernyshov Никита ёЁ',
  fullTime: '2:20',

  article: 'newArticleФЁ',
  time: '7:29',
  amount: 333,
  boxes: 111,
  inBox: 3,
  rateWeekday: 202,
  rateWeekend: 404
});

describe('Article model', () => {
  before((done) => {
    mongoose.models = {};
    // mongoose.modelSchemas = {};
    // mongoose.connection.removeAllListeners('open');
    done();
  });

  it('all correct', (done) => {
    const article = new Article(correctArticleData.toObject());

    const error = article.validateSync();
    expect(error).to.equal(undefined);

    done();
  });

  it('NO parameter updateDate!', (done) => {
    const data = correctArticleData;
    delete data.updateDate;

    const article = new Article(data);
    const error = article.validateSync();

    expect(error.errors.updateDate.message)
      .to.equal('Path `updateDate` is required.');

    done();
  });

  it('NO parameter date!', (done) => {
    const data = correctArticleData;
    delete data.date;

    const article = new Article(data);
    const error = article.validateSync();

    expect(error.errors.date.message)
      .to.equal('Path `date` is required.');

    done();
  });

  it('nameId INCORRECT! Number failed', (done) => {
    const article = new Article({
      ...correctArticleData,
      nameId: 'ass'
    });

    const error = article.validateSync();
    expect(error.errors.nameId.message)
      .to.equal('Cast to Number failed for value "ass" at path "nameId"');

    done();
  });

  it('name INCORRECT!', (done) => {
    const article = new Article({
      ...correctArticleData,
      name: 'Chernyshov Никита ёЁ 2222'
    });

    const error = article.validateSync();
    expect(error.errors.name.message)
      .to.equal('nameIncorrect');

    done();
  });

  it('fullTime INCORRECT!', (done) => {
    const article = new Article({
      ...correctArticleData,
      fullTime: '2-20'
    });

    const error = article.validateSync();
    expect(error.errors.fullTime.message)
      .to.equal('fullTimeIncorrect');

    done();
  });

  it('article INCORRECT!', (done) => {
    const article = new Article({
      ...correctArticleData,
      article: 'newArticleФЁ23 @#!'
    });

    const error = article.validateSync();
    expect(error.errors.article.message)
      .to.equal('articleIncorrect');

    done();
  });

  it('time INCORRECT!', (done) => {
    const article = new Article({
      ...correctArticleData,
      time: '90:29--'
    });

    const error = article.validateSync();
    expect(error.errors.time.message)
      .to.equal('timeIncorrect');

    done();
  });

  it('amount INCORRECT! Number failed', (done) => {
    const article = new Article({
      ...correctArticleData,
      amount: 'wrong'
    });

    const error = article.validateSync();
    expect(error.errors.amount.message)
      .to.equal('Cast to Number failed for value "wrong" at path "amount"');

    done();
  });

  it('boxes INCORRECT! Number failed', (done) => {
    const article = new Article({
      ...correctArticleData,
      boxes: 'wrong'
    });

    const error = article.validateSync();
    expect(error.errors.boxes.message)
      .to.equal('Cast to Number failed for value "wrong" at path "boxes"');

    done();
  });

  it('inBox INCORRECT! Number failed', (done) => {
    const article = new Article({
      ...correctArticleData,
      inBox: 'wrong'
    });

    const error = article.validateSync();
    expect(error.errors.inBox.message)
      .to.equal('Cast to Number failed for value "wrong" at path "inBox"');

    done();
  });

  it('rateWeekday INCORRECT! Number failed', (done) => {
    const article = new Article({
      ...correctArticleData,
      rateWeekday: 'wrong'
    });

    const error = article.validateSync();
    expect(error.errors.rateWeekday.message)
      .to.equal('Cast to Number failed for value "wrong" at path "rateWeekday"');

    done();
  });

  it('rateWeekend INCORRECT! Number failed', (done) => {
    const article = new Article({
      ...correctArticleData,
      rateWeekend: 'wrong'
    });

    const error = article.validateSync();
    expect(error.errors.rateWeekend.message)
      .to.equal('Cast to Number failed for value "wrong" at path "rateWeekend"');

    done();
  });
});
