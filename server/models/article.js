import mongoose from 'mongoose';

const articleSchema = new mongoose.Schema({
  updateDate: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  nameId: {
    type: Number,
    required: true,
    trim: true,
    match: [/^[0-9]{1,10}$/, 'nameIdIncorrect']
  },
  name: {
    type: String,
    required: true,
    trim: true,
    match: [/^[a-zA-zа-яА-ЯёЁ ]{1,40}$/, 'nameIncorrect']
  },
  fullTime: {
    type: String,
    required: true,
    trim: true,
    match: [/^[0-9:]{1,10}$/, 'fullTimeIncorrect']
  },

  article: {
    type: String,
    required: true,
    match: [/^[a-zA-Zа-яА-Я0-9ёЁ ]{1,40}$/, 'articleIncorrect']
  },
  time: {
    type: String,
    required: true,
    trim: true,
    match: [/^[0-9.:]{1,10}$/, 'timeIncorrect']
  },
  amount: {
    type: Number,
    required: true,
    trim: true,
    match: [/^[0-9]{1,10}$/, 'amountIncorrect']
  },
  boxes: {
    type: Number,
    required: true,
    trim: true,
    match: [/^[0-9]{1,10}$/, 'boxesIncorrect']
  },
  inBox: {
    type: Number,
    required: true,
    trim: true,
    match: [/^[0-9]{1,10}$/, 'inBoxIncorrect']
  },
  plusBox: Number,
  rateWeekday: {
    type: Number,
    required: true,
    trim: true,
    match: [/^[0-9]{1,10}$/, 'rateWeekdayIncorrect']
  },
  rateWeekend: {
    type: Number,
    required: true,
    trim: true,
    match: [/^[0-9]{1,10}$/, 'rateWeekendIncorrect']
  }
});

export default mongoose.model('Article', articleSchema);
