import article from './article';
import mongoose from 'mongoose';
import { expect, should } from 'chai';
import Immutable from 'immutable';
import supertest from 'supertest';

import config from '../config/config';
import Article from './../models/article';
import {
  startDatabase,
  closeDatabase
} from '../database/index';
import app from '../server.js';

should();

let server;
let request;

const correctArticleData = Immutable.Map({
  updateDate: 'updateDate',
  date: 'date',
  nameId: 123,
  name: 'Chernyshov Никита ёЁ',
  fullTime: '2:20',

  article: 'newArticleФЁ',
  time: '7:29',
  amount: 333,
  boxes: 111,
  inBox: 3,
  rateWeekday: 202,
  rateWeekend: 404
});


describe('Article API (routes)', () => {
  describe('create', () => {
    beforeEach(() => {
      try {
        mongoose.models = {};

        const port = config.server.port;

        // console.log(port);
        server = app.listen(port);
        // console.log('listen');

        // console.log(app);
        request = supertest(server);
        Article.remove({});
      } catch (error) {
        console.log('ERROR Article API, create, before()');
      }
    });

    beforeEach(() => {
      Article.remove({});
      server.close();
    });
    // afterEach(async () => await closeDatabase());


    it('get all articles', (done) => {
      console.log('START');
      request
        .get('/article')
        .end((err, res) => {
          if (err) throw err;

          // console.log(res);
          // console.log(res.status);
          console.log(res.body);
          expect(res.status).to.equal(200);
          expect(res.body).to.eql({});
          done();
        });
    });
  });
});

//     // it('NO parameter updateDate!', (done) => {
//     //   const data = correctArticleData;
//     //   delete data.updateDate;

//     //   const article = new Article(data);
//     //   const error = article.validateSync();

//     //   expect(error.errors.updateDate.message)
//     //     .to.equal('Path `updateDate` is required.');

//     //   done();
//     // });
//   });

//   // describe('delete', () => {
//   //   let article;

//   //   before((done) => {
//   //     mongoose.models = {};
//   //     // add new article and save it
//   //     article = new Article(correctArticleData.toObject());
//   //     const saveError = article.save();
//   //     expect(saveError.message).to.equal(undefined);

//   //     done();
//   //   });

//   //   it('all correct', (done) => {
//   //     console.log(article._id);

//   //     // expect(saveError).to.equal(undefined);
//   //     done();
//   //   });
//   // });
// });