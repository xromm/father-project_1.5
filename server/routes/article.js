import Article from './../models/article';

export default {
  get: async (ctx, next) => {
    console.log('get by id');
    ctx.body = await Article.findById(ctx.params.id);
  },
  getall: async (ctx, next) => {
    console.log('get all');
    ctx.body = await Article.find({});
  },
  create: async (ctx) => {
    console.log('create');
    ctx.body = await new Article(ctx.body).save();
  },
  update: async (id, article) => {
    console.log('update article');
  },
  delete: async (_id) => {
    console.log('delete article');
    console.log(_id);
    const res = await Article.remove(_id);
    console.log(res);
  }
};
