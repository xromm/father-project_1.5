import Koa from 'koa';
import bodyparser from 'koa-bodyparser';
import logger from 'koa-logger';
// import logger from './middleware/logger';
import error from './middleware/error';
import article from './middleware/article';
import config from './config/config';

import { startDatabase } from './database';

const url = config.db.url;

const app = new Koa()
  .use(logger())
  .use(error);

app
  .use(bodyparser())
  .use((ctx, next) => {
    ctx.body = ctx.request.body;
    next();
  });

app
  .use(article);

app.use((ctx) => {
  ctx.body = 'Not Found, Sorry =(';
});

startDatabase(url);

export default app;
