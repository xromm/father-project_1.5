var path = require('path');
var webpack = require('webpack');

const NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = {
  devtool: NODE_ENV === 'development' ? 'eval' : 'cheap-module-eval-source-map',

  watch: NODE_ENV === 'development',
  watchOptions: {
    aggregateTimeout: 100
  },

  entry: [
    'webpack-hot-middleware/client',
    'babel-polyfill',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),

    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV),
      LANG: JSON.stringify('ru'),
    })
  ],
  resolve: {
    moduleDirectories: ['node_modules'],
    extensions: ['', '.js', '.jsx']
  },
  resolveLoader: {
    modulesDirecrories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extenstions: ['', 'js']
  },
  module: { //Обновлено
    preLoaders: [ //добавили ESlint в preloaders
      {
        exclude: [
          path.resolve(__dirname, "node_modules"),
          path.resolve(__dirname, "server"),
        ],
        test: /\.jsx?$/,
        loaders: ['eslint'],
        include: [
          path.resolve(__dirname, "src"),
        ],
      }
    ],
    loaders: [ //добавили babel-loader
      {
        exclude: [
          path.resolve(__dirname, "node_modules"),
        ],
        loaders: ['babel-loader'],
        include: [
          path.resolve(__dirname, "src"),
        ],
        test: /\.jsx?$/,
        plugins: ['transform-runtime'],
      }
    ]
  },
  postcss: function() {
    return [autoprefixer, precss];
  },
};


if (NODE_ENV == 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  )
};
